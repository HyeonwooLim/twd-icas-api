package twd.icas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import icasApi.ICASConfig;
import icasApi.common.CommRecord;
import twd.icas.config.ChannelConfiguration;
import twd.icas.config.FrontTypeProperties;
import twd.icas.config.FrontTypeProperties.IcasProperties;
import twd.icas.exceptions.InterfaceClientException;
import twd.icas.exceptions.InterfaceClientException.InterfaceType;

public abstract class BaseIcasService implements IcasService{
	private Logger logger = LoggerFactory.getLogger(ChannelConfiguration.ICAS_IO_LOGGER_NAME);
	
	private FrontTypeProperties properties;
	private String frontTypeName;
	private String logPath;
	
	public BaseIcasService(String frontTypeName, FrontTypeProperties properties) {
		this.frontTypeName = frontTypeName;
		this.properties = properties;
		this.logPath = properties.getIcasLogPath();
	}

	protected IcasProperties getProperties() {
		if(properties.getFrontTypeName().containsKey(frontTypeName)){
			return properties.getMediaType(frontTypeName).getIcas();
		}else {
			InterfaceClientException exception = new InterfaceClientException("INF9001", InterfaceType.ICAS);
			exception.setDebugMessage("front bff type name not exist [" + frontTypeName + "]");
			throw exception;
		}
	}
	
	protected String getLogPath() {
		return this.logPath;
	}
	
	@Override
	public List<Map<String, String>> callServiceList(String method, Map<String, String> param) throws InterfaceClientException {
		
		try {
			IcasResponse response = call(method, param);
			
			if (response.getStatus() == ICASConfig.SUCCESS) {
				return response.getResponse();
			}else {
				InterfaceClientException exception = new InterfaceClientException("INF9001", InterfaceType.ICAS);
				exception.setOrgExceptionCode(String.valueOf(response.getStatus()));
	
				if (response.getStatus() == ICASConfig.PARAM_ERROR) {
					exception.setOrgMessage("icas param error");
					exception.setDebugMessage("icas param error");
				}else if (response.getStatus() == ICASConfig.CONNECT_ERROR) {
					exception.setOrgMessage("icas connection error");
					exception.setDebugMessage("icas connection error");
				}else if (response.getStatus() == ICASConfig.NO_RECORD_DATA) {
					exception.setOrgMessage("icas no record data error");
					exception.setDebugMessage("icas no record data error");
				}else {
					exception.setOrgMessage("icas unknown error");
					exception.setDebugMessage("icas unknown error");
				}
				throw exception;				
			}
		}finally {
			release();
		}
	}

	@Override
	public Map<String, String> callService(String method, Map<String, String> param) throws InterfaceClientException{
		return callServiceList(method, param)
				.stream()
				.findFirst()
				.orElse(new HashMap<String,String>());
	}
	
	@Override
	public int executeService(String method, Map<String, String> param) {		
		try {
			IcasResponse response = call(method, param);
			return response.getStatus();
		}finally {
			release();
		}
	}
	
	@Override
	public IcasResponse call(String method, Map<String, String> param) {
		CommRecord rec = new CommRecord();
		param.forEach((k,v) -> rec.addValue(k, v));
		addParam(method, rec);
		logger.trace("{} icas param : {}", getType(), param );
		IcasResponse response = callIcas();
		logger.trace("{} icas reponse: status={}, data={}", getType(), response.getStatus(), response.getResponse());
		return response;
	}
	
	protected abstract void addParam(String method, CommRecord record);
	protected abstract IcasResponse callIcas();
	protected abstract void release();
}
