package twd.icas;

import icasApi.common.CommDebug;



/**
 *
 *
 * <pre>
 * ICAS 관련 상수 값들을 정의 해 놓은 클래스로 ICAS 인증 정보와 에러코드 상수 등이 정의되어 있다.
 * </pre>
 *
 * <pre>
 * <작업 History>
 *
 * 2006. 7. 11. : 성일경, 최초작성
 * </pre>
 *
 * @author 성일경
 *
 *
 */

/*
 * 2012-05-31 new
 * WBSM1287     qh5196so	NjsjNTsuLSozL2I4
 * 
 * WBSM1000		pc3622sd	NjsjNTsuLSoxLWIx
 */
public class IcasConst
{  
    /**
     *  로그 경로 설정
     */
	//TODO: 로그를 물리 파일로 쌓는 부분에 대해 결정 필요
    public static final String ICAS_LOG_PATH = "/";
    
    /**
     * 사이트 코드
     */
    public static final String EST_SITE_CD = "est";

    /**
     * e-Station 관련 ICAS 인증 ID
     */
    public static final String ICAS_EST_ID = "WBSM1000";

    /**
     * e-Station 관련 ICAS 인증 Password
     */
    public static final String ICAS_EST_PWD = "1d418858a3094f1c";

    /**
     * tworld 관련 ICAS 인증 key 2009 08 ipin 적용으로인하여 인증 키 추가 
     */
    public static final String ICAS_EST_KEY = "ec9f3e2ad2f241da80204c6f9ced9c7d";
    
    /**
     * tworld 관련 ICAS 인증 key 2009 08 ipin 적용으로인하여 인증 키 추가 
     */
    public static final String ICAS_EST_KEY_ID = "A";
    
    /**
     * ICAS 인증 ID (mobile)
     */
    public static final String ICAS_MTW_ID = "WBSM1287";    
    
    /**
     * ICAS 인증 Password (mobile)
     * ICAS 보안강화에 따라 변경. 2016.11 (8자리 --> 16자리)
     */
    //public static String ICAS_WZN_PWD = IcasInfoMapper.getInstance().getData("CPKEY_PWD");
    public static String ICAS_MTW_PWD = "24094ee5e32ed048";
    
    /**
     * ICAS 인증 CP key (mobile)
     * ICAS 보안강화에 따라 변경. 2016.11 (16자리 --> 32자리)
     */ 
    //public static String ICAS_WZN_CPKEY = IcasInfoMapper.getInstance().getData("CPKEY");
    public static String ICAS_MTW_CPKEY = "01b8bfb03f5147cab8401d0c77e0166e";
    
    /**
     * ICAS 인증 CP key id (mobile)
     * ICAS 보안강화에 따라 추가. 2016.11
     */
    //public static String ICAS_WZN_CPKEY_ID = IcasInfoMapper.getInstance().getData("CPKEY_ID");
    public static String ICAS_MTW_CPKEY_ID = "A";
    
    /**
     * e-Station Password 인증 성공
     */
    public static final int AUTH_EST_PWD_SUCCESS = 3455;

    /**
     * e-Station Password 인증 실패
     */
    public static final int AUTH_EST_PWD_FAIL = 3456;

    /**
     * 필수 파라메터가 넘어오지 않음
     */
    public static final int REQUIRED_PARAMETER_IS_EMPTY = 3156;

    /**
     * 회선 정보에 대한 결과 값이 없음
     */
    public static final int AEP_DB_NO_ROW_SELECTED_CUSTOMERS = 3452;

    /**
     * e-Station 패스워드를 조회할 수 없음
     */
    public static final int AEP_SVCSEC_NOT_ACCESSABLE = 3453;

    /**
     * e-Station 패스워드와 일치하는 서비스관리번호가 없음
     */
    public static final int AEP_DB_NO_ROW_SELECTED_SVCSEC = 3454;    
    
    /**
     * U-OTP 가입 코드
     */
    public static final int UOTP_ENTRANCE_CD = 3464;
    
    
    /**
     * ICAS API Dubug Level
     * CommDebug.LOG_TRACE : Trace Log  출력
     * CommDebug.LOG_NONE  : Log 출력 안함
     */
    public static final int ICAS_LOG_Level = CommDebug.LOG_NONE;
    
    /**
     * ICAS 인자 사용자 ID
     */
    public static final String ICAS_IN_USERID  = "USER_ID";
    
    /**
     * ICAS 인자 비밀번호
     */
    public static final String ICAS_IN_PWD  = "PWD";
    
    
    /**
     * ICAS 인자 사이트코드
     */
    public static final String ICAS_IN_SITECD  = "SITE_CD";
    
    /**
     * ICAS 인자 서비스번호
     */
    public static final String ICAS_IN_SVCNUM  = "SVC_NUM";
    
    /**
     * ICAS 인자 서비스관리 번호 권한 플래그
     */
    public static final String ICAS_IN_SMNFLAG  = "SMN_FLAG";   
    
    /**
     * ICAS 인자 사용자 이름
     */
    public static final String ICAS_IN_MBRNM  = "MBR_NM";
    
    /**
     * ICAS 인자 서비스관리번호
     */
    public static final String ICAS_IN_SVCMGMTNUM  = "SVC_MGMT_NUM";
    
    /**
     * ICAS 인자 주민번호
     */
    public static final String ICAS_IN_CTZCORPNUM  = "CTZ_CORP_NUM";
    
    /**
     * ICAS 인자 중복가입자정보
     */
    public static final String ICAS_IN_DUPSCRBINFO  = "DUP_SCRB_INFO";
    
    /**
     * ICAS 인자 실명인증여부
     */
    public static final String ICAS_IN_RNMCHECKYN  = "RNM_CHECK_YN";
    
    /**
     * ICAS 인자 메일주소
     */
    public static final String ICAS_IN_EMAILADDR  = "EMAIL_ADDR";
    
    /**
     * ICAS 인자 14세미만 부모동의 여부
     */
    public static final String ICAS_IN_PRENTAGREEYN  = "PRENT_AGREE_YN";
        
    /**
     * ICAS 인자 DMB 서비스관리번호
     */
    public static final String ICAS_IN_RELSVCMGMTNUM  = "REL_SVC_MGMT_NUM";
    
    /**
     * ICAS 인자 주민번호
     */
    public static final String ICAS_OUT_CNCTZCORPNUM  = "CN_CTZ_CORP_NUM ";
    
    /**
     * ICAS 인자 인증성공여부(YES:일치, NO:불일치)
     */
    public static final String ICAS_OUT_AUTH  = "AUTH ";
    
    /**
     * ICAS 인자 패스워드
     */
    public static final String ICAS_OUT_PWD  = "PWD ";

    /**
     * ICAS 인자 서비스코드
     */
    public static final String ICAS_OUT_SVCCD  = "SVC_CD";

    /**
     * ICAS 인자 서비스번호
     */
    public static final String ICAS_OUT_SVCNUM  = "SVC_NUM";
    
    /**
     * ICAS 인자 사이트 연계정보
     */
    public static final String ICAS_IN_CNNTINFO  = "CNNT_INFO";
    
    /**
     * ICAS 인자 주민번호추출 성별코드
     */
    public static final String ICAS_IN_SSNSEXCD  = "SSN_SEX_CD";
    
    /**
     * ICAS 인자 주민번호추출 생년월일
     */
    public static final String ICAS_IN_SSNBIRTHDT  = "SSN_BIRTH_DT";
    
    /**
     * ICAS 인자 상품코드
     */
    public static final String ICAS_IN_PRODID  = "PROD_ID";

    /**
     * 2014-08-07 추가
     * ICAS 공통 본인이 사용중인 아이디 입니다.
     */
    public static final int AD_DUP_ID_SELF  = 3448;
    
    /**
     * 조회된 웹회원 정보가 없습니다. (IUK_ZORD_SVC TABLE에 SVC_NUM 에 해당하는 데이터 존재하지만
     * CGNR_COM_MBR_SITE, CGNR_MULTI_SVC TABLE 에 데이터가 없는 경우)
     */
    public static final int DB_NO_SELECTED_MEMBERS = 3163;
    
    /**
     * 2014-08-07 추가
     * 조회된 회선 정보가 없습니다.(IUK_ZORD_SVC TABLE에 SVC_NUM 에 해당하는 데이터가 없는 경우)
     */
    public static final int DB_NO_SELECTED_CUSTOMERS = 3162;
    
    /**
     * 사이트 코드
     */
    public static final String WZN_SITE_CD = "wzn";
    
    /**
     * ICAS 공통 사용자변경시 변경할 데이터가 존재하지 않습니다. 모두 일치 시
     */
    public static final int NOT_EXIST_CHG_DATA  = 3325;
    
    /**
     * ICAS 인자 
     */
    public static final int DB_NO_ROW_SELECTED_CUSTOMERS  = 3101;
    
    /**
     * 해당 사이트에 가입되어 있습니다.
     */
    public static final int AMSJ_JOIN = 3464;
    
    /**
     * 명의자 주민번호 일치(Y)
     */
    public static final int RGSTNUM_EQUAL = 3436;
    /////////////////////////////////////////////////////////////////////
    

    //////////////////////////////GetMembersSV////////////////////////////////////////       
    /**
     * ICAS 인자 GetMembersSV 사업자번호
     */
    public static final String MEMBER_OUT_CMWMCTZCORPNUM = "CMWM_CTZ_CORP_NUM";
    /**
     * ICAS 인자 GetMembersSV 아이디
     */
    public static final String MEMBER_OUT_CMWMUSERID = "CMWM_USER_ID";
    /**
     * ICAS 인자 GetMembersSV 사용자 이름
     */
    public static final String MEMBER_OUT_CMWMMBRNM = "CMWM_MBR_NM";
    /**
     * ICAS 인자 GetMembersSV 멤버 번호
     */
    public static final String MEMBER_OUT_CMWMMBRNUM = "CMWM_MBR_NUM";
    /**
     * ICAS 인자 GetMembersSV 주민번호 성별
     */
    public static final String MEMBER_OUT_CMWMSSNSEXCD = "CMWM_SSN_SEX_CD";
    /**
     * ICAS 인자 GetMembersSV 주민번호 생년월일
     */
    public static final String MEMBER_OUT_CMWMSSNBIRTHDT = "CMWM_SSN_BIRTH_DT";
    /**
     * ICAS 인자 GetMembersSV 멤버 채널 아이디
     */
    public static final String MEMBER_OUT_CMWSMBRCHLID = "CMWS_MBR_CHL_ID"; 
    //////////////////////////////////////////////////////////////////////
   
    //////////////////////////////GetSiteRegInfosSV///////////////////////////////////////  
    /**
     * ICAS 인자 GetSiteRegInfosSV 사이트 코드
     */    
    public static final String SITEREGINFO_OUT_CMWSSITECD = "CMWS_SITE_CD";
    /**
     * ICAS 인자 GetSiteRegInfosSV 아이디
     */    
    public static final String SITEREGINFO_OUT_CMWSUSERID = "CMWS_USER_ID";
    /**
     * ICAS 인자 GetSiteRegInfosSV 서비스 관리번호
     */    
    public static final String SITEREGINFO_OUT_CMWSSVCMGMTNUM = "CMWS_SVC_MGMT_NUM";
    //////////////////////////////////////////////////////////////////////
           
    /////////////////////////GetCustomersSV/////////////////////////////////////////////    
    /**
     * ICAS 인자 GetCustomersSV 서비스 관리 번호
     */   
    public static final String CUSTOMER_OUT_ZSVCSVCMGMTNUM = "ZSVC_SVC_MGMT_NUM";
    /**
     * ICAS 인자 GetCustomersSV 회선의 명의자 주민(버인)번호
     */
    public static final String CUSTOMER_OUT_ZCSTCTZCORPNUM = "ZCST_CTZ_CORP_NUM";
    /**
     * ICAS 인자 GetCustomersSV 서비스 이용 종류 코드
     */
    public static final String CUSTOMER_OUT_SVCTYPCD = "SVC_TYP_CD";
    /**
     * ICAS 인자 GetCustomersSV 고객유형코드
     */
    public static final String CUSTOMER_OUT_ZCSTCUSTTYPCD = "ZCST_CUST_TYP_CD";
    /**
     * ICAS 인자 GetCustomersSV 고객상세유형코드
     */
    public static final String CUSTOMER_OUT_ZCSTCUSTDTLTYPCD = "ZCST_CUST_DTL_TYP_CD";
    /**
     * ICAS 인자 GetCustomersSV 회선의 명의자(법인) 명
     */
    public static final String CUSTOMER_OUT_ZCSTCUSTNM  = "ZCST_CUST_NM";
    /**
     * ICAS 인자 GetCustomersSV 서비스 상태 코드
     */
    public static final String CUSTOMER_OUT_SVCSTCD  = "SVC_ST_CD";
    /**
     * ICAS 인자 GetCustomersSV 요금상품 ID
     */
    public static final String CUSTOMER_OUT_FEEPRODID  = "FEE_PROD_ID";
    /**
     * ICAS 인자 GetCustomersSV ESTATION 신청동의여부
     */
    public static final String CUSTOMER_OUT_ESTATIONAGREEYN  = "ESTATION_AGREE_YN";
    /**
     * ICAS 인자 GetCustomersSV 서비스비밀번호 상태코드
     */
    public static final String CUSTOMER_OUT_PWDSTCD  = "PWD_ST_CD";   
    /**
     * ICAS 인자 GetCustomersSV 중복가입자정보
     */   
    public static final String CUSTOMER_OUT_ZCSTDUPSCRBINFO = "ZCST_DUP_SCRB_INFO";    
    /**
     * ICAS 인자 GetCustomersSV 사이트 연계정보
     */   
    public static final String CUSTOMER_OUT_ZCSTCNNTINFO = "ZCST_CNNT_INFO";    
    /**
     * ICAS 인자 GetCustomersSV 성별
     */
    public static final String CUSTOMER_OUT_ZCSTSSNSEXCD = "ZCST_SSN_SEX_CD";
    /**
     * ICAS 인자 GetCustomersSV 생년월일
     */
    public static final String CUSTOMER_OUT_ZCSTSSNBIRTHDT = "ZCST_SSN_BIRTH_DT";     
    /**
     * ICAS 인자 GetCustomersSV 요금상품ID
     */   
    public static final String CUSTOMER_OUT_ZFEEPRODID = "FEE_PROD_ID";
    /**
     * ICAS 인자 GetCustomersSV  단말기모델코드
     */   
    public static final String CUSTOMER_OUT_EQPMDLCD = "EQP_MDL_CD";    
    //////////////////////////////////////////////////////////////////////

    /////////////////////////GetMultiCircuitSV/////////////////////////////////////////////    
    /**
     * ICAS 인자 GetMultiCircuitSV 서비스 관리 번호
     */   
    public static final String MULTISVC_OUT_SVCMGMTNUM = "MSVC_SVC_MGMT_NUM";
    /**
     * ICAS 인자 GetMultiCircuitSV 서비스 다서비스 순번
     */   
    public static final String MULTISVC_OUT_REPSVCSEQ = "REP_SVC_SEQ";
    /**
     * ICAS 인자 GetMultiCircuitSV 서비스 다서비스 서비스번호
     */   
    public static final String MULTISVC_OUT_SVCNUM = "SVC_NUM";
    //////////////////////////////////////////////////////////////////////
    
    //////////////////////////////GetIpinFrRealRgstSV////////////////////////////////////////       
    /**
     * ICAS 인자 GetIpinFrRealRgstSV 사이트 연계정보
     */
    public static final String IPIN_OUT_CNNTINFO = "CNNT_INFO";
    /**
     * ICAS 인자 GetIpinFrRealRgstSV 중복가입자정보
     */
    public static final String IPIN_OUT_DUPSCRBINFO = "DUP_SCRB_INFO";
  
    //////////////////////////////////////////////////////////////////////
    
    //////////////////////////////GetRealNameAuthSV////////////////////////////////////////       
    /**
     * ICAS 인자 GetRealNameAuthSV 사이트 연계정보
     */
    public static final String CERT_OUT_CMWMCNNTINFO = "CMWM_CNNT_INFO";
    /**
     * ICAS 인자 GetRealNameAuthSV 중복가입자정보
     */
    public static final String CERT_OUT_CMWMDUPSCRBINFO = "CMWM_DUP_SCRB_INFO";
    /**
     * ICAS 인자 GetRealNameAuthSV 성별
     */
    public static final String CERT_OUT_CMWMSSNSEXCD = "CMWM_SSN_SEX_CD";
    /**
     * ICAS 인자 GetRealNameAuthSV 생년월일
     */
    public static final String CERT_OUT_CMWMSSNBIRTHDT = "CMWM_SSN_BIRTH_DT"; 
    /**
     * ICAS 인자 GetRealNameAuthSV 
     */
    public static final String CERT_OUT_CMWMRNMCHECKYN = "CMWM_RNM_CHECK_YN";
    
    
    //////////////////////////////TRXUpdateMemberPwdSV////////////////////////////////////////       
    /**
     * ICAS 인자 TRXUpdateMemberPwdSV 생년월일
     */
    public static final String ICAS_IN_SSN_BIRTH_DT = "SSN_BIRTH_DT";    
    
    /**
     * ICAS 인자 TRXUpdateMemberPwdSV 서비스번호 
     */
    public static final String ICAS_IN_SVC_NUM = "SVC_NUM";    
    
  
    //////////////////////////////////////////////////////////////////////

    /**
     * [20140515 SKT회원정책변경] ICAS 인자 스패머 해제여부(X  : 현상태 유지, N  : 스패머 해제)
     */
    public static final String ICAS_IN_SPAM_YN = "SPAM_YN";
    
    /**
     * [20140515 SKT회원정책변경] ICAS 인자 휴면계정 해제 여부(X  : 현상태 유지, N  : 휴면계정 해제)
     */
    public static final String ICAS_IN_SLEEP_MBR_YN = "SLEEP_MBR_YN";

    
//@@@PTD_S
    /**
     * 주민번호 제거 프로젝트 cust_num 고객번호  추가 
     */
    public static final String CUSTOMER_OUT_ZCSTCUSTNUM = "ZCST_CUST_NUM";

    /**
     * 주민번호 제거 프로젝트 실사용자 di  추가 
     */
    public static final String CUSTOMER_OUT_CNDUPSCRBINFO = "CN_DUP_SCRB_INFO";

    /**
     * 주민번호 제거 프로젝트 명의자 di  추가 
     */
    public static final String CUSTOMER_OUT_MNDUPSCRBINFO = "MN_DUP_SCRB_INFO";
    
    /**
     * 주민번호 제거 프로젝트 고객명   추가 
     */
    public static final String ICAS_IN_CUSTNM = "CUST_NM";       
//@@@PTD_E
    
    
    /** 201511 TID START */
    
    /**
     * TID (통합아이디) 회원채널식별자 추가 
     */
    public static final String ICAS_IN_MBRCHLID = "MBR_CHL_ID"; 
    
    /**
     * TID (통합아이디) 수신여부 추가 
     */
    public static final String ICAS_IN_RCVAGREEYN = "RCV_AGREE_YN";
    
    /**
     * TID (통합아이디) 수신동의일자 추가 
     */
    public static final String ICAS_IN_RCVAGREEDTM = "RCV_AGREE_DTM";

    /**
     * TID (통합아이디) T아이디 T world군 회원 가입일자 추가 
     */
    public static final String ICAS_IN_FSTCREDTM = "FST_CRE_DTM";

    /**
     * TID (통합아이디) 최종로그인사이트코드 추가 
     */
    public static final String ICAS_IN_LAST_LOGON_SITE_CD = "LAST_LOGON_SITE_CD";

    /** 201511 TID END */
    
    //////////////////////// GetMinorLegalAgentSV ///////////////////////
    /**
     * 법정대리인 고객번호
     */
    public static final String ICAS_OUT_REL_CUST_NUM = "REL_CUST_NUM";
    
    /**
     * 법정대리인 서비스 번호
     */
    public static final String ICAS_OUT_REL_SVC_NUM = "REL_SVC_NUM";
    
    /**
     * 법정대리인 서비스 관리번호
     */
    public static final String ICAS_OUT_REL_SVC_MGMT_NUM = "REL_SVC_MGMT_NUM";
    //////////////////////// GetMinorLegalAgentSV ///////////////////////
    
}
