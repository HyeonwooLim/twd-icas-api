package twd.icas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import icasApi.common.CommPair;
import icasApi.common.CommRecord;

public class IcasResponse {
	private int status;
	private List<Map<String, String>> response;
	
	public IcasResponse(int status, @SuppressWarnings("rawtypes") Vector list) {
		this.status = status;
		this.response = new ArrayList<>();
		for(int i = 0; i < list.size(); i++) {
			CommRecord record = (CommRecord)list.get(i);
			Map<String, String> resultMap = new HashMap<>();
			for(int j = 0; j < record.size(); j++) {
				CommPair pair = record.at(j);
				resultMap.put(pair.name, pair.value);
			}
			response.add(resultMap);
		}
	}
	
	public int getStatus() {
		return status;
	}

	public List<Map<String, String>> getResponse() {
		return response;
	}

}
