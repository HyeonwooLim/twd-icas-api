package twd.icas;

import java.util.List;
import java.util.Map;

import twd.icas.exceptions.InterfaceClientException;

public interface IcasService {
	IcasTypes getType();
	List<Map<String, String>> callServiceList(String method, Map<String, String> param) throws InterfaceClientException;
	Map<String, String> callService(String method, Map<String, String> param) throws InterfaceClientException;	
	int executeService(String method, Map<String, String> param);
	IcasResponse call(String method, Map<String, String> param);
}
