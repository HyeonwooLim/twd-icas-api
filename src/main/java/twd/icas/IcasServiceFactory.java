package twd.icas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import twd.icas.config.FrontTypeProperties;
import twd.icas.impl.AuthIcasService;
import twd.icas.impl.ExecIcasService;
import twd.icas.impl.GetIcasService;
import twd.icas.impl.SmartIcasService;

@Component
public class IcasServiceFactory {
	
	@Autowired
	private FrontTypeProperties properties;
	
	public IcasService create(String frontTypeName, IcasTypes type) {
		switch(type){
			case AUTH_ICAS: 
				return new AuthIcasService(frontTypeName, properties);
			case GET_ICAS:
				return new GetIcasService(frontTypeName, properties);
			case EXEC_ICAS:
				return new ExecIcasService(frontTypeName, properties);
			case SMART_ICAS:
				return new SmartIcasService(frontTypeName, properties);
		}
		return null;
	}
}
