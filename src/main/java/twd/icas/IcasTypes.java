package twd.icas;

public enum IcasTypes {
	/** GetICAS API */
	GET_ICAS,
	/** AuthICAS API */
	AUTH_ICAS,
	/** ExecICAS API */
	EXEC_ICAS, 
	/** SmtICAS API */
	SMART_ICAS
}
