package twd.icas;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import twd.icas.config.FrontTypeProperties;
import twd.icas.exceptions.InterfaceClientException;
import twd.icas.exceptions.InterfaceClientException.InterfaceType;

/**
 * Icas, legacy 채널 연동처리 delegator
 * @author ahnhojung
 *
 */
@Component
public class LegacyInterfaceDelegator {

	@Autowired
	private FrontTypeProperties properties;


	@Autowired
	private IcasServiceFactory icasFactory;

	/** 멤버채널ID */
	public static final String HTTP_HEADER_KEY_MBR_CHANNEL_ID = "TWD-MbrChlId";
	
	/** 입력 채널의 Channel Name 을 전달하기 위한 key */
	public static final String HTTP_HEADER_KEY_CHANNEL_NAME = "TWD-ChannelName";
    
	/**
	 * front channel별 처리를 위해 request header에 입력된 bff-name정보를 조회
	 * @return bffname bff name
	 */
	private String getBffName() {
		ServletRequestAttributes servletRequest = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(servletRequest != null) {
			return StringUtils.defaultString(servletRequest.getRequest().getHeader(
					HTTP_HEADER_KEY_CHANNEL_NAME),
					"twd-bff-web");
		}else{
			return "twd-bff-web";
		}
	}


	/**
	 * application.yml에 정의된 bff name 정보가 있는지 조회하여 등록된 bff-name이 없는 경우 exception 발생
	 * @param bffName
	 * @throws InterfaceClientException
	 */
	private void findFrontTypeName(final String bffName) throws InterfaceClientException{
		if(!properties.getFrontTypeName().containsKey(bffName)) {
			InterfaceClientException exception = new InterfaceClientException("INF9001", InterfaceType.SWING);
			exception.setDebugMessage("front bff type name not exist [" + bffName + "]");
			throw exception;
		}
	}

	/**
	 * ICAS service call
	 * @param frontTypeName bff name
	 * @param method Icas method name
	 * @param param Icas param
	 * @param type Icas 호출유형 ( GetIcas, SmtIcas )
	 * @return response map
	 */
	public Map<String, String> callIcasService(final String method, final Map<String, String> param, final IcasTypes type) throws InterfaceClientException{
		IcasService service = icasFactory.create(getBffName(), type);
		return service.callService(method, param);
	}

	/**
	 * ICAS service call
	 * @param frontTypeName bff name
	 * @param method Icas method name
	 * @param param Icas param
	 * @param type Icas 호출유형 ( GetIcas, SmtIcas )
	 * @return response list
	 */
	public List<Map<String, String>> callIcasServiceList(final String method, final Map<String, String> param, final IcasTypes type) throws InterfaceClientException{
		IcasService service = icasFactory.create(getBffName(), type);
		return service.callServiceList(method, param);
	}

	/**
	 * ICAS service call
	 * @param frontTypeName bff name
	 * @param method Icas method name
	 * @param param Icas param
	 * @param type Icas 호출유형
	 * @return IcasResponse
	 */
	public IcasResponse callIcas(final String method, final Map<String, String> param, final IcasTypes type){
		IcasService service = icasFactory.create(getBffName(), type);
		return service.call(method, param);
	}

	/**
	 * ICAS service execute
	 * @param frontTypeName bff name
	 * @param method Icas method name
	 * @param param Icas param
	 * @param type Icas 호출유형 ( AuthIcas, ExecIcas )
	 * @return response list
	 */
	public int executeIcasService(final String method, final Map<String, String> param, final IcasTypes type){
		IcasService service = icasFactory.create(getBffName(), type);
		return service.executeService(method, param);
	}


}
