package twd.icas.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *  Icas, legacy 채널 configuration
 * @author ahnhojung
 *
 */
@Configuration
@EnableConfigurationProperties(value = { FrontTypeProperties.class})
public class ChannelConfiguration {
	public static final String ICAS_IO_LOGGER_NAME = "icasio";

}
