package twd.icas.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import twd.icas.IcasTypes;

@ConfigurationProperties(prefix = "tdirect")
public class FrontTypeProperties {
	/** tworld 입력매체별 legacy channel 정보 */
	private Map <String, TypeProperties> frontTypeName;
	/** icas log path */
	private String icasLogPath;

	public Map<String, TypeProperties> getFrontTypeName() {
		return frontTypeName;
	}

	public void setFrontTypeName(Map<String, TypeProperties> frontTypeName) {
		this.frontTypeName = frontTypeName;
	}

	public TypeProperties getMediaType(String key) {
		return frontTypeName.get(key);
	}

	public String getIcasLogPath() {
		return icasLogPath;
	}

	public void setIcasLogPath(String icasLogPath) {
		this.icasLogPath = icasLogPath;
	}

	public static class TypeProperties{
		/** icas 채널 정보 */
		private IcasProperties icas = new IcasProperties();

		public IcasProperties getIcas() {
			return icas;
		}
		public void setIcas(IcasProperties icas) {
			this.icas = icas;
		}
	}

	public static class IcasProperties{
		/** icas ip */
		private Map<IcasTypes, String >serviceIp;
		/** icas id */
		private String estId;
		/** icas password */
		private String estPassword;
		/** icas key */
		private String estKey;
		/** icas key id */
		private String estKeyId = "A";

		public Map<IcasTypes, String> getServiceIp() {
			return serviceIp;
		}
		public void setServiceIp(Map<IcasTypes, String> serviceIp) {
			this.serviceIp = serviceIp;
		}
		public String getEstId() {
			return estId;
		}
		public void setEstId(String estId) {
			this.estId = estId;
		}
		public String getEstPassword() {
			return estPassword;
		}
		public void setEstPassword(String estPassword) {
			this.estPassword = estPassword;
		}
		public String getEstKey() {
			return estKey;
		}
		public void setEstKey(String estKey) {
			this.estKey = estKey;
		}
		public String getEstKeyId() {
			return estKeyId;
		}
		public void setEstKeyId(String estKeyId) {
			this.estKeyId = estKeyId;
		}
	}
}
