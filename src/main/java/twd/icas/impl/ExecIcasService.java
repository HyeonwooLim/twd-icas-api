package twd.icas.impl;

import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import icasApi.ExecICAS;
import icasApi.common.CommRecord;
import twd.icas.BaseIcasService;
import twd.icas.IcasResponse;
import twd.icas.IcasTypes;
import twd.icas.config.FrontTypeProperties;

public class ExecIcasService extends BaseIcasService{
	private IcasTypes type = IcasTypes.EXEC_ICAS;
	
	private ExecICAS icas;
	private String ip;
	
	public ExecIcasService(String frontTypeName, FrontTypeProperties properties) {
		super(frontTypeName, properties);
		this.icas = new ExecICAS(
				getProperties().getEstId(), 
				getProperties().getEstPassword(),
				getProperties().getEstKey(), 
				getProperties().getEstKeyId());
		this.icas.setLogPath(getLogPath());
		this.ip = (getProperties().getServiceIp()!=null)? getProperties().getServiceIp().get(IcasTypes.EXEC_ICAS) : null;
		
	}
	
	@Override
	public IcasTypes getType() {
		return type;
	}

	@Override
	protected void addParam(String method, CommRecord record) {
		icas.addMethod(method);
		icas.addParam(record);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected IcasResponse callIcas() {
		int status = StringUtils.isEmpty(ip) ? icas.call() : icas.call(ip);
		return new IcasResponse(status, new Vector());
	}

	@Override
	protected void release() {
		icas.clear();
	}


}
