package twd.icas.impl;

import org.apache.commons.lang.StringUtils;

import icasApi.GetICAS;
import icasApi.common.CommRecord;
import twd.icas.BaseIcasService;
import twd.icas.IcasResponse;
import twd.icas.IcasTypes;
import twd.icas.config.FrontTypeProperties;

public class GetIcasService extends BaseIcasService{
	private IcasTypes type = IcasTypes.GET_ICAS;
	
	private GetICAS icas;
	private String ip;
	
	public GetIcasService(String frontTypeName, FrontTypeProperties properties) {
		super(frontTypeName, properties);
		this.icas = new GetICAS(
				getProperties().getEstId(), 
				getProperties().getEstPassword(),
				getProperties().getEstKey(), 
				getProperties().getEstKeyId());
		this.icas.setLogPath(getLogPath());
		this.ip = (getProperties().getServiceIp()!=null)? getProperties().getServiceIp().get(IcasTypes.GET_ICAS) : null;
	}
	
	@Override
	public IcasTypes getType() {
		return type;
	}

	@Override
	protected void addParam(String method, CommRecord record) {
		icas.addMethod(method);
		icas.addParam(record);
	}

	@Override
	protected IcasResponse callIcas() {
		int status = StringUtils.isEmpty(ip) ? icas.call() : icas.call(ip);
		return new IcasResponse(status, icas.vcRetVal);
	}

	@Override
	protected void release() {
		icas.clear();
	}


}
