package twd.icas.impl;

import org.apache.commons.lang.StringUtils;

import icasApi.SmtICAS;
import icasApi.common.CommRecord;
import twd.icas.BaseIcasService;
import twd.icas.IcasResponse;
import twd.icas.IcasTypes;
import twd.icas.config.FrontTypeProperties;

public class SmartIcasService extends BaseIcasService{
	private IcasTypes type = IcasTypes.SMART_ICAS;
	
	private SmtICAS icas;
	private String ip;
	
	public SmartIcasService(String frontTypeName, FrontTypeProperties properties) {
		super(frontTypeName, properties);
		this.icas = new SmtICAS(
				getProperties().getEstId(), 
				getProperties().getEstPassword(),
				getProperties().getEstKey(), 
				getProperties().getEstKeyId());
		this.icas.setLogPath(getLogPath());
		this.ip = (getProperties().getServiceIp()!=null)? getProperties().getServiceIp().get(IcasTypes.SMART_ICAS) : null;
	}
	
	@Override
	public IcasTypes getType() {
		return type;
	}

	@Override
	protected void addParam(String method, CommRecord record) {
		icas.addMethod(method);
		icas.addParam(record);
	}

	@Override
	protected IcasResponse callIcas() {
		int status = StringUtils.isEmpty(ip) ? icas.call() : icas.call(ip);
		return new IcasResponse(status, icas.vcRetVal);
	}

	@Override
	protected void release() {
		icas.clear();
	}


}
