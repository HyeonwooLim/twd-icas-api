package com.skt.twd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import lombok.extern.slf4j.Slf4j;
import twd.icas.AppConfig;
import twd.icas.ConfigProfiles;

@SpringBootApplication
@Slf4j
@Profile({ConfigProfiles.LOCAL})
public class TwdApiCoreApplicationTests implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TwdApiCoreApplicationTests.class, args);
	}

	@Autowired
	private AppConfig appConfig;

	@Override
	public void run(String... args) throws Exception {
		log.info("run ... {}", appConfig);
	}
}
