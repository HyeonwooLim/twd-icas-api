package com.skt.twd.cache.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

/**
 * 컴포넌트 메타 정보
 *
 * @author P128160
 * @since 2018.12.05
 * @see
 * <pre>
 * 수정이력
 * 2018.12.05 P128160 파일생성
 * </pre>
 */
@RedisHash("CmsComponent")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CmsComponentEntityVo {

    @Id
    private String cacheKey;

    private String id;
    private Integer version;
    private String name;
    private String channel;
    private String dataType;
    private String schema;
    private String api;
    private String pcHtml;
    private Integer pcWidth;
    private Integer pcHeight;
    private String mobileHtml;
    private Integer mobileWidth;
    private Integer mobileHeight;
}
