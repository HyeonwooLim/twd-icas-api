package com.skt.twd.core.utils.old;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import cryptix.provider.key.RawSecretKey;
import cryptix.util.core.Hex;
import lombok.extern.slf4j.Slf4j;
import xjava.security.Cipher;

@Slf4j
public class CpCipherManager {

	/**
	 * Tworld 암호화 Instance
	 */
    public static CpCipherManager instance;

	/**
	 * Tworld 암호화 Key
	 */
	private final static String MKEY 			= "303030303032333230303830"; // 변경 [2008-09-26]

    private final static String MKEY_TPOINT	= "203030303032333230305892"; // T포인트 제휴 암호화 키 (20151001)

    private static final Charset DEFAULT_CHARSET = Charset.forName("EUC-KR");

	/**
	 * Tworld 암호화 getInstance
	 */
    public static CpCipherManager getInstance() {
    	if ( instance == null ) {
        	java.security.Security.addProvider( new cryptix.provider.Cryptix() );
            instance = new CpCipherManager();
        }
        return instance;
    }

    /**
     *
     * <pre>
     * HEX Type 데이터를 암호화 된 데이터로 변환
     * </pre>
     *
     * @param input
     * @return
     */
    public  String encrypt(String input) {
        byte[] ect = null;
        String message = "";

        try {
            message = new String(input.getBytes(DEFAULT_CHARSET), StandardCharsets.ISO_8859_1);
            /***** 메세지 길이 체크(8의 배수여야 한다) *****/
            while( (message.length() % 8) != 0 ) message += " ";

            Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
            RawSecretKey key = new RawSecretKey("DES_EDE3", MKEY.getBytes());

            alg.initEncrypt(key);
            ect = alg.crypt(message.getBytes("8859_1"));
			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + ect);
			}
        } catch ( Exception e ) {
				log.error(e.getMessage(), e);
        }
        return Hex.toString(ect);
    }

    /**
     *
     * <pre>
     * 암호화 된 데이터를 원래의 HEX Type으로 복호화
     * </pre>
     *
     * @param input
     * @return
     */
    public  String decrypt(String input) {
        byte[] dct = null;

        try {
            Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
            RawSecretKey key = new RawSecretKey("DES_EDE3", MKEY.getBytes());

            alg.initDecrypt(key);
            dct = alg.crypt(Hex.fromString(input));

			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + dct);
			}


        } catch ( Exception e ) {
				log.error(e.getMessage(), e);
        }
        return new String(dct).trim();
    }

    /**
     *
     * <pre>
     * HEX Type 데이터를 암호화 된 데이터로 변환
     * </pre>
     *
     * @param input
     * @param in
     * @return
     */
    public  String encrypt(String input, String in) {
        byte[] ect = null;
        String message = "";

        try {
            message = new String(input.getBytes("euc-kr"),"8859_1");
            /***** 메세지 길이 체크(8의 배수여야 한다) *****/
            while( (message.length() % 8) != 0 ) message += " ";

            Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
            RawSecretKey key = new RawSecretKey("DES_EDE3", in.getBytes());

            alg.initEncrypt(key);
            ect = alg.crypt(message.getBytes("8859_1"));
			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + ect);
			}
        } catch ( Exception e ) {
				log.error(e.getMessage(), e);
        }
        return Hex.toString(ect);
    }

    /**
     *
     * <pre>
     * 암호화 된 데이터를 원래의 HEX Type으로 복호화
     * </pre>
     *
     * @param input
     * @param in
     * @return
     */
    public  String decrypt(String input, String in) {
    	return decrypt(input, in, DEFAULT_CHARSET);
    }

    /**
    *
    * <pre>
    * 암호화 된 데이터를 원래의 HEX Type으로 복호화
    * </pre>
    *
    * @param input
    * @param in
    * @return
    */
   public  String decrypt(String input, String in, Charset charset) {
       byte[] dct = null;

       try {
           Cipher alg = Cipher.getInstance("DES-EDE3", "Cryptix");
           RawSecretKey key = new RawSecretKey("DES_EDE3", in.getBytes());

           alg.initDecrypt(key);
           dct = alg.crypt(Hex.fromString(input));

			if ( log.isDebugEnabled() ) {
				log.debug("## DECRYPT : DES-EDE3 - Cryptix : " + dct);
			}


       } catch ( Exception e ) {
				log.error(e.getMessage(), e);
       }
       return new String(dct, charset).trim();
   }

	public static String getMkeyTpoint() {
		return MKEY_TPOINT;
	}


}
